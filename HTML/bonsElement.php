<?php 

$elements = [
    'Alimentaire' => [ 
        'Alimentaire value 1', 
        'Alimentaire value 2', 
        'Alimentaire value 3', 
        'Alimentaire value 4', 
        'Alimentaire value 5', 
    ],
    'Technologie' => [ 
        'Tech value 1', 
        'Tech value 2', 
        'Tech value 3', 
    ],
];

// get category from request
$category = $_GET['category'];

// will contain decoded json
$result = json_encode($elements[$category]);

echo $result;
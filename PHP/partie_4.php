<?php 

/**
 * Testing 2 functions
 */
class Test {

    // will contain a string to be used within check method
    public $chain = '';

    // will be used within min_max method
    public $tableau_chaine = [];

    /**
    * check if the given string is valid 
    * @return boolean
    */
    public function check() {

        $var = $this->chain;

        $var = trim($var);
    
        // should not be null and should not be greater than 1000
        if(!isset($var) || strlen($var) == 0 || strlen($var) > 1000) {
            return false;
        }
    
        // split string into array of characters
        $arrayOfCharacters = str_split($var);
    
        if(count($arrayOfCharacters) % 2 != 0) {
            return false;
        }
    
        // get first part
        $first_part = '';
        for ($i = 0; $i < count($arrayOfCharacters) ; $i++) { 
    
            if($arrayOfCharacters[$i] == '(' || $arrayOfCharacters[$i] == '[') {
                $first_part .= $arrayOfCharacters[$i];
            } else {
                // go out
                $i = count($arrayOfCharacters);
            }
    
        }
    
    
        // get last index of first part
        $lastIndex = strlen($first_part) - 1;
    
    
        // count the first part
        $firstPartLength = strlen($first_part);
    
        // reverse the first part
        $firstPartExploded = str_split($first_part);
        
        $reversedPart = '';
        for ($i=$firstPartLength - 1; $i >= 0 ; $i--) { 
            if($firstPartExploded[$i] == '(')  {
                $reversedPart .= ')';
            } else if($firstPartExploded[$i] == '[') {
                $reversedPart .= ']';
            } else {
                //echo 'STOP HERE';
                return false;
            }
        }
    
        $fullPart = implode('', $firstPartExploded) . $reversedPart;
    
    
        // split string and check full part and splited part
        $splitedPartFromString = substr($var, 0, strlen($fullPart));
    
        echo '<br>step: '.$splitedPartFromString .' and '. $fullPart ;
    
        if(trim($splitedPartFromString) == trim($fullPart)) {
            $otherPart = substr($var, strlen($fullPart));
            if(strlen($otherPart)) {

                // update chain
                $this->chain = trim($otherPart);
                return $this->check();
            }
    
            return true;
        } else {
            return false;
        }
    
    }

   public function min_max() {
        //
   }

}

$test = new Test();

$test->chain = '[()][]()()';

if($test->check()){
    echo '<br>valid';
} else {
    echo '<br>not valid';
}
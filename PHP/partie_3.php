<?php
echo '
<style>
table, td {
border: 1px solid black;
border-collapse: collapse;
}

td {
    width: 25px;
    height: 25px;
    padding: 15px
}
</style>
';
echo '<table>';

$horizontal = 0;

$i = 1;
while ($horizontal < 10) {
    
    echo '<tr><td>'. $i.'</td>';

    $j = $i;
    $vertical = 0;
    while ($vertical < 9) {
    
        $num = $j + $i;
        echo '<td>'. $num.'</td>';
    
    
        $j = $num;
        $vertical++;
    
    }

    echo '</tr>';
    $i++;
    $horizontal++;

}

echo '</table>';
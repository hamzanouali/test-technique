<?php 

/**
 * recursive method to check wether a given string is valid 
 * @param {String} $var
 * @return boolean
 */
function check($var) {

    $var = trim($var);

    // should not be null and should not be greater than 1000
    if(!isset($var) || strlen($var) == 0 || strlen($var) > 1000) {
        return false;
    }

    // split string into array of characters
    $arrayOfCharacters = str_split($var);

    if(count($arrayOfCharacters) % 2 != 0) {
        return false;
    }

    // get first part
    $first_part = '';
    for ($i = 0; $i < count($arrayOfCharacters) ; $i++) { 

        if($arrayOfCharacters[$i] == '(' || $arrayOfCharacters[$i] == '[') {
            $first_part .= $arrayOfCharacters[$i];
        } else {

            if($i == 0 && $arrayOfCharacters[$i] == ')') {
                return false;
            } else if($i == 0 && $arrayOfCharacters[$i] == ']') {
                return false;
            }

            // go out
            $i = count($arrayOfCharacters);
        }

    }


    // get last index of first part
    $lastIndex = strlen($first_part) - 1;


    // count the first part
    $firstPartLength = strlen($first_part);

    // reverse the first part
    $firstPartExploded = str_split($first_part);
    
    $reversedPart = '';
    for ($i=$firstPartLength - 1; $i >= 0 ; $i--) { 
        if($firstPartExploded[$i] == '(')  {
            $reversedPart .= ')';
        } else if($firstPartExploded[$i] == '[') {
            $reversedPart .= ']';
        } else {
            //echo 'STOP HERE';
            return false;
        }
    }

    $fullPart = implode('', $firstPartExploded) . $reversedPart;


    // split string and check full part and splited part
    $splitedPartFromString = substr($var, 0, strlen($fullPart));

    echo '<br>step: '.$splitedPartFromString .' and '. $fullPart ;

    if(trim($splitedPartFromString) == trim($fullPart)) {
        $otherPart = substr($var, strlen($fullPart));
        if(strlen($otherPart)) {
            return check(trim($otherPart));
        }

        return true;
    } else {
        return false;
    }

}

if(!isset($_GET['chaine'])) {
    echo 'Please enter a string to : http://127.0.0.1/test-technique/PHP/partie_1.php?chaine=()[](]';
} else {
    if(check($_GET['chaine'])){
        echo '<br>valid';
    } else {
        echo '<br>not valid';
    }
    
}

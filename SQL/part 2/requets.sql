/* part 1 ------------------------- */

SELECT
product.product_id AS "id",
product_category.name AS "nom de categorie",
product.name AS "nom de produit",
COUNT(order_product.order_id) AS "quantité"

from product 
inner JOIN product_category on product.product_category_id = product_category.product_category_id
inner join order_product ON order_product.product_id = product.product_id 
INNER JOIN purchase_order ON purchase_order.order_id = order_product.order_id

WHERE purchase_order.DATE = CURDATE()
GROUP BY order_product.order_id;

/* part 2 -------------------------*/

SELECT customer.* FROM customer 
INNER JOIN purchase_order ON customer.customer_id = purchase_order.customer_id
INNER JOIN order_product ON purchase_order.order_id = order_product.order_id
INNER JOIN product ON product.product_id = order_product.product_id
INNER JOIN product_category ON product_category.product_category_id = product.product_category_id

WHERE product_category.NAME = "tech" AND purchase_order.DATE >= "2019-01-01" AND purchase_order.DATE = "2019-06-15";

/* part 3 -------------------------*/

/* using joins */

SELECT 
product_category.*,
COUNT(product.product_category_id) AS "nombre de produits" 

FROM product_category
inner JOIN product ON product.product_category_id = product_category.product_category_id
GROUP BY product.product_category_id;



/* using subqueries */
SELECT 
product_category.*,
(select COUNT(*) FROM product WHERE product_category.product_category_id = product.product_category_id) AS "nombre de produits" 

FROM product_category;







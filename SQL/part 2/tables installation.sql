USE testtech;

CREATE TABLE customer (

	customer_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(50) NOT NULL,	
	lastname VARCHAR(50) NOT null,
	register_date DATE,
	birth_date DATE,
	address VARCHAR(255) NOT null,	
	zipcode CHAR(5) NOT null,
	city VARCHAR(50) NOT null,
	phone_number VARCHAR(20) NOT NULL
);

CREATE TABLE purchase_order (
	order_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	customer_id INTEGER UNSIGNED,
	date DATE,
	
	FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

CREATE TABLE product_category (
	product_category_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50),
	description VARCHAR(255)
);

CREATE TABLE product (
	product_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	product_category_id INTEGER UNSIGNED,
	name VARCHAR(50),
	description VARCHAR(255),
	price DECIMAL(5,2),
	available_stock INTEGER,
	
	FOREIGN KEY (product_category_id) REFERENCES product_category(product_category_id)
);

CREATE TABLE order_product (
	order_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	product_id INTEGER UNSIGNED,
	
	FOREIGN KEY (product_id) REFERENCES product(product_id)
);









